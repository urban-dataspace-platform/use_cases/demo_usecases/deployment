
# Demo Use-Cases

## Projektbeschreibung

In diesem Repository liegt das Deployment folgender Demo Use-Cases:
- [sensor.community (ehemals luftdaten.info)](https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/air_quality_ldinfo)
- [OpenSenseMap](https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/air_quality_osm)
- [Deutscher Wetterdienst](https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/weather_dwd)
- [OpenWeatherMap](https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/weather_owm)

Der Use-Cases wurde auf der FIWARE-basierten [Urban Dataspace Platform](https://gitlab.com/urban-dataspace-platform/core-platform) entwickelt und integriert.

## Installation

Die Installation dieser Use-Cases setzt eine bestehende Installation der [Core Platform](https://gitlab.com/urban-dataspace-platform/core-platform) voraus.

Die bestehende `inventory.yml` wird um die Werte aus `inventory.default` ergänzt. Einige Werte müssen dabei wie im Abschnitt **Konfiguration** beschrieben angepasst werden.

Danach können die Demo Use-Cases durch ausführen der `playbook.yml` installiert werden.

## Konfiguration

Die einzelnen Use-Cases haben mehrere Optionen gemeinsam. Die Standardwerte sollten für die meisten Deployments funktionieren:
- `enable`      -   ob der Use-Case installiert werden soll
- `name`        -   Name des Use-Case
- `git_url`     -   URL des Repositories, in dem der NodeRED Flow liegt
- `flow_name`   -   Name des NodeRED Flows in dem repository
- `dataspace`   -   Name des Datenraumes, der von dem Use-Case verwendet wird

Use-Case spezifische Konfiguration:
- `air_quality_ldinfo.sensor_id` - ID des Sensors, kann von [der Karte](https://sensor.community) nach Auswahl eines Sensors abgelesen werden
- `air_quality_osm.place` - Name des Ortes, von dem Sensorwerte abgefragt werden sollen
- `weather_dwd.station_id` - ID der Wetterstation, kann aus [dieser Liste](https://www.dwd.de/EN/ourservices/met_application_mosmix/mosmix_stations.html) entnommen werden
- `weather_owm.station_id` - ID der Stadt für die Wettervorhersage. Kann nach [suchen einer Stadt](https://openweathermap.org/city/2855745) aus der URL entnommen werden.
- `weather_owm.api_key` - API-Key für OpenWeatherMap

## Kontaktinformationen

Dieses Projekt wurde von der [Hypertegrity AG](https://www.hypertegrity.de/) entwickelt.

## Link zum Original-Repository

https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/weather_owm